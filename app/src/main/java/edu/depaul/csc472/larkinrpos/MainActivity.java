package edu.depaul.csc472.larkinrpos;

import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    Button newOrder;//for new orders
    Button nextItem;//to take next request
    Button total;
    AutoCompleteTextView itemName;
    EditText quantity;
    EditText unitPrice;
    TextView displayTotal;
    TextView orderedItems;
    NextActionListener userRequest;

    static final String[] predefinedFoods = new String[]{"apple", "orange", "banana", "kiwi", "mango" };
    static final Double[] predefinedFoodsPrices = new Double[]{0.50, 0.68, 0.39, 1.25, 0.89};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, predefinedFoods);
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.itemName);
        autoCompleteTextView.setAdapter(arrayAdapter);

        newOrder = (Button) findViewById(R.id.newOrder);
        nextItem = (Button) findViewById(R.id.nextItem);
        itemName = (AutoCompleteTextView) findViewById(R.id.itemName);
        quantity = (EditText) findViewById(R.id.quantity);
        unitPrice = (EditText) findViewById(R.id.unitPrice);
        total = (Button) findViewById(R.id.total);
        displayTotal = (TextView) findViewById(R.id.displayTotal);
        orderedItems = (TextView) findViewById(R.id.orderedItems);
        userRequest = new NextActionListener();


        int[] actionRequests = {R.id.newOrder, R.id.nextItem, R.id.total};

        for(int request: actionRequests){
            View desiredEffect = findViewById(request);
            desiredEffect.setOnClickListener(userRequest);
        }


        itemName.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event){
                boolean action = false;
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    String desiredFood = itemName.getText().toString();
                    for(int index = 0; index < predefinedFoods.length; index++){
                        if(predefinedFoods[index].toUpperCase().equals(desiredFood.toUpperCase())){
                            unitPrice.setText(predefinedFoodsPrices[index].toString());
                            action = true;

                            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(itemName.getWindowToken(), 0);
                            return action;
                        }
                    }
                }
                return action;
            }
        });

    }

    private class NextActionListener implements View.OnClickListener{
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.newOrder:
                    quantity.setText("1");
                    unitPrice.setText("0.00");
                    displayTotal.setText("0.00");
                    orderedItems.setText("");
                    itemName.setText("");
                    break;
                case R.id.nextItem:
                    quantity.setText("1");
                    unitPrice.setText("0.00");
                    itemName.setText("");
                    break;
                case R.id.total:
                    String newItem = itemName.getText().toString();
                    orderedItems.setText(newItem);

                    int itemQuantity = Integer.parseInt(quantity.getText().toString());
                    double itemUnitPrice = Double.parseDouble(unitPrice.getText().toString());
                    double tempTotal = itemQuantity * itemUnitPrice;
                    displayTotal.setText(String.valueOf(tempTotal));

                    break;
            }


        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
